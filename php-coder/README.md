# Конвертер файлов

Допустим, что у некоего клиента есть проблема: поступающие в огромном количестве его сотрудникам текстовые файлы приходят в разных кодировках, и сотрудники тратят много времени на перекодирование («ручной подбор кодировки»). Соответственно, он хотел бы иметь инструмент, позволяющий автоматически приводить кодировки всех текстовых файлов к некоей одной. Итак, на свет появляется проект с кодовым названием «Конвертер файлов».

Исходные форматы: plain text, HTML, MD.<br/>
Исходные кодировки: CP1251, UTF8, CP866, KOI8R.<br/>
Целевая кодировка: UTF8