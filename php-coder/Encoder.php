<?php

require_once('FileLogger.php');

/*
    Класс-перекодиовщик файлов из одной кодировки в другую.
*/
class Encoder
{
    const TARGET_DIR_RIGHTS = 0700; //Права доступа к директории выходных файлов
    const MAX_FILE_SIZE = 5 * 1024 * 1024; //максимальный размер файла в байтах (5 МБ)
    const  ENCODINGS = ['utf-8', 'cp1251', 'cp866', 'KOI8-R']; //Исходные кодировки

    private $sourceDir;     //Директория с файлами, которые необходимо перекодировать
    private $targetDir;     //Директория, куда будут сохранены файлы после перекодировки
    private $logger;        //Объект класса FileLogger


    /**
     * Конструктор класса. Создает объект логгера
     *
     * @param String $sourceDir - директория с ихсодными файлами. По умолчанию - './srcFiles'
     *
     * @param String $targetDir - директория для выходных файлов. По умолчанию - './outFiles'
     * 
     * @param String $logFileName - Имя лог-файла. По умолчанию 'file.log'
     *
     *  @throws Exception
     */
    public function __construct($sourceDir = './srcFiles', $targetDir = './outFiles', $logFileName = 'file.log')
    {

        //Исходная директория не должна совпадать с выходной
        if($sourceDir == $targetDir){
            throw new Exception("sourceDir must not match targetDir");
        }

        $sourceDir = is_null($sourceDir) ? './srcFiles' : $sourceDir;
        $targetDir = is_null($targetDir) ? './outFiles' : $targetDir;
        $logFileName = is_null($logFileName) ? 'file.log' : $logFileName;

        $this->sourceDir = $sourceDir;
        $this->targetDir = $targetDir;

        //создаем логгер
        $this->logger = FileLogger::create($logFileName);
    }

    /**
     * Меняет кодировку, если она из ENCODINGS, у всех файлов в указанной директории на UTF-8
     *
     *  @throws Exception
     */
    public function changeEncoding()
    {   
        //Если исходной директории не существует, бросить исключение
        if (!file_exists($this->sourceDir)) {
            throw new Exception("Directory '$this->sourceDir' does not exist");
        }

        //Сканируем директорию на файлы
        $files = scandir($this->sourceDir);

        //Если не удалось сканировать, бросить исключение
        if (!$files) {
            throw new Exception("Error scanning directory: $this->sourceDir");
        }

        //Для каждого файла из директории
        foreach ($files as $file) {
        
            //Формируем путь до файла
            $srcFilePath = $this->sourceDir . '/' . $file;

            //Передаем файл в конструтор класса SplFileInfo для получения информации
            $info = new SplFileInfo($srcFilePath);

            //Если тип файла text/plain или расширение html или md
            if (
                mime_content_type($srcFilePath) == 'text/plain' ||
                strtolower($info->getExtension()) == 'html' ||
                strtolower($info->getExtension()) == 'md'
            ) {

                $srcFileSize = filesize($srcFilePath);

                //Пропускаем файлы с большим размером
                if($srcFileSize > self::MAX_FILE_SIZE){
                    $this->logger.log('Размер файла $file превышает максимально допустимый!');
                    continue;
                }

                //Получаем содержимое файла
                $content = file_get_contents($srcFilePath);

                if (!$content) {
                    die('При получении содержимого файла произошла ошибка!');
                }

                //Получаем кодировку содержимого файла
                $encoding = mb_detect_encoding($content, self::ENCODINGS);

                if (!$encoding) {
                    die('Не удалось определить кодировку для файла: ' . $file);
                }

                //Кодируем в UTF-8 из кодировки $code
                $new_content = iconv($encoding, 'UTF-8', $content);

                if (!$new_content) {
                    die('При преобразовании кодировки произошла ошибка!');
                }

                if (!file_exists($this->targetDir)) {
                    mkdir($this->targetDir, self::TARGET_DIR_RIGHTS);
                }

                //Выводим данные о подходящем файле
                echo "name: $file; size: $srcFileSize bytes, encoding: $encoding\n";
                
                $dstFilePath = $this->targetDir . '/' . $file;

                //Выводим содержимое обратно в файл
                $count = file_put_contents($dstFilePath, $new_content);

                if (!$count) {
                    die('Ошибка при записи данных в файл!');
                }

                //Вывод подробной информации в файл-логгер
                $this->logger->log("Входной файл: $srcFilePath; Размер: $srcFileSize bytes, Исходная кодировка: $encoding; Выходной файл: $dstFilePath; Выходная кодировка: UTF-8");
            }
        }
    }
}