<?php
/*
    Класс-логгер для вывода сообщений в лог-файл
*/
class FileLogger
{
    const LOG_DIR_PATH = './log/';          //\\        Путь до директории логов
    const LOG_DIR_RIGHTS = 0700;           //  \\       Права директории логов
                                          //    \\
    private $file;                       //      \\     Открытый файл
    private $lines = [];                //========\\    Накапливаемые строки
    static public $loggers = [];       //          \\   Массив всех созданных-журналов
    private $time;                    //            \\  Время создания объекта


    /**
     * Приватный конструктор класса. Создает файл логов с помощью метода create
     *
     * @param String $fname имя файла логгера. По умолчанию - 'file.log'
     *
     */
    private function __construct($fname)
    {
        if (!file_exists(self::LOG_DIR_PATH)) {
            mkdir(self::LOG_DIR_PATH, self::LOG_DIR_RIGHTS);
        }

        if (is_null($fname)) {
            $fname = 'file.log';
        }

        if (!$this->file = fopen(self::LOG_DIR_PATH . $fname, "a+")) {
            die('Failed to open the log file! ... ' . error_get_last());
        }

        $this->time = microtime(true);
    }

    /**
     *  Открытый метод для создания объектов класса
     *
     * @param String $fname имя файла логгера.
     *
     */
    public static function create($fname)
    {
        //Если объект уже существует, возвращаем его.
        if (isset(self::$loggers[$fname])) {
            return self::$loggers[$fname];
        }

        //Иначе, создаем новый объект и возвращаем его.
        return self::$loggers[$fname] = new self($fname);
    }

    /**
     *  Деструктор класс. Освобождает ресурсы
     */
    public function __destruct()
    {
        //Выводим все накопленные строки в журнал
        fputs($this->file, implode("", $this->lines));

        //Закрываем открытый файл
        fclose($this->file);
    }

    /**
     *  Метод добавляет в вывод переданные данные. Накопленные строки выводятся в файл не сразу,
     *  а при завершении скрипта.
     *
     * @param String $str - данные для вывода в файл
     */
    public function log($str)
    {
        //Каждая строка предваряется текущей датой.
        $prefix = "[" . date("Y-m-d h:i:s") . "]: ";

        $str = preg_replace('/^/m', $prefix, rtrim($str));
        $this->lines[] = $str . "\n";
    }
}
