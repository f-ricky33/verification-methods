<?php
require_once('FileLogger.php');
require_once('Encoder.php');

/**
 * Печатает на консоль help
 *
 */
function printHelp()
{
    echo "
        1. -srcdir   path/to/dir    Specify the source directory
        2. -dstdir   path/to/dir    Specify the destination directory
        3. -logfile  filename       Specify the name of the log file
        4. -default                 srcdir = 'srcFiles', dstdir = 'outFiles', logfile = 'file.log'
    ";
}


/**
 * Получает все переданные параметры в массив params
 *
 * @param Array $argv - массив переданных аргументов
 *
 */
function getConsoleParams($argv)
{

    $params = [];

    foreach ($argv as $key => $arg) {

        // Очищаем введенные пользователем данные от точек, слэшей
        // Чтобы не дать возможности выбраться из текущей директории
        $arg = str_replace(['.', '\\', '/'], '', $arg);
        $argv[$key + 1] = str_replace(['.', '\\', '/'], '', $argv[$key + 1]);

        if ($arg == '-srcdir') {
            $params[$arg] = $argv[$key + 1];
        }

        if ($arg == '-dstdir') {
            $params[$arg] = $argv[$key + 1];
        }

        if ($arg == '-logfile') {
            $params[$arg] = $argv[$key + 1] . '.log';
        }

        if ($arg == '-default') {
            $params[$arg] = true;
        }
    }

    return $params;
}


/**
 * Точка входа в программу. Обрабатывает аргументы и вызывает необходимые методы.
 *
 * @param Array $argv - массив переданных аргументов
 * 
 */
function startFunc($argv)
{

    //Если аргументов не передано -> выдать help
    if (!$argv[1]) {
        printHelp();
        die();
    }

    //Получаем аргументы из консоли
    $params = getConsoleParams($argv);

    //Кодируем необходимые файлы
    try {
        if ($params['-default']) {
            $encoding = new Encoder();
            $encoding->changeEncoding();
        } else {
            $encoding = new Encoder($params['-srcdir'] ? $params['-srcdir'] : NULL, $params['-dstdir'] ? $params['-dstdir'] : NULL, $params['-logfile'] ? $params['-logfile'] : NULL);
            $encoding->changeEncoding();
        }
    } catch (\Throwable $th) {
        echo $th;
    }
}

startFunc($argv);
